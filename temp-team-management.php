<?php /* Template Name: Team Member Management */ ?>
<?php get_header(); ?>
<div id="mainContent">
	<h1 class="int">Len Batterson</h1>
	<p class="backOnePage"><a href="/Pages/Team" title="Back to Portfolio">Back to Team</a></p>

	<div class="contentBG clearfix" id="bioLanding">
		<div class="leftColumn">
			<img class="bioImage" src="/assets/root/images/staff/len03_resize235x235.jpg" alt="bio picture">
			<h2>Chairman, Chief Executive Officer</h2>
			<div class="fullBio">
				<div>Len is Chairman and CEO of Batterson Venture Capital, LLC. A pioneering venture capitalist and entrepreneur, Len has invested in and founded many venture-backed companies, entrepreneurial enterprises and venture capital investment organizations since 1982.</div>
				<div><br></div>
				<div>Len founded and served as Chairman and CEO of Batterson Venture Partners, LLC organized in 1995. BVP was one of the first venture capital firms open to accredited investors. &nbsp;Len went on to form and serve as Chairman and CEO of Batterson Cross Zakin, LLC, which was launched in 2004. These pioneering firms were the forefront of early stage venture capital investing, specificially tailored to individual investors. &nbsp;From 1989 – 2002, Len was the managing general partner of Batterson, Johnson &amp; Wang, L.P., one of the first private venture capital firms organized in Illinois to focus on early-stage high technology and life science companies. &nbsp;From 1985 to 1987, Len was the director of the venture capital division of the Allstate Insurance Company, one of the nation's oldest, largest and most successful venture capital management operations. While at Allstate, he was responsible for more than $350 million in invested or committed capital, which at the time was one of the largest pools of venture capital in the U.S. &nbsp;Len played an integral role in the financing and restructuring of Control Video Corporation, which later became America Online, Inc. (AOL/Time Warner - NYSE: TWX). Prior to Allstate, Len specialized in the turnaround, restart and revitalization of various entrepreneurial companies as principal of Leonard Batterson Associates.</div>
			</div>
		</div>
		<div class="rightColumn">
			<h3 class="moduleHeader">Management</h3>
			<div class="md clearfix">
				<a class="linkImg" href="/Team/Len+Batterson?cat=Management" title="Read More About Len Batterson">
					<img src="/assets/root/images/staff/len03_resize116x116.jpg" alt="Len Batterson bio picture">
					<span class="view">view</span>
				</a>
				<strong>Len Batterson</strong>
				<span class="pos">Chairman, Chief Executive Officer</span>
				<br>
				<a class="readmore" href="/Team/Len+Batterson?cat=Management" title="Read More About Len Batterson">Read More</a>
			</div>
			<div class="md clearfix">
				<a class="linkImg" href="/Team/James+Vaughan?cat=Management" title="Read More About James Vaughan">
					<img src="/assets/root/images/staff/jim02_resize116x116.jpg" alt="James Vaughan bio picture">
					<span class="view">view</span>
				</a>
				<strong>James Vaughan</strong>
				<span class="pos">Managing Principal</span><br>
				<a class="readmore" href="/Team/James+Vaughan?cat=Management" title="Read More About James Vaughan">Read More</a>
			</div>
			<div class="md clearfix">
				<a class="linkImg" href="/Team/Annie+Piotrowski?cat=Management" title="Read More About Annie Piotrowski">
					<img src="/assets/root/images/staff/annie02_resize116x116.jpg" alt="Annie Piotrowski bio picture">
					<span class="view">view</span>
				</a>
				<strong>Annie Piotrowski</strong>
				<span class="pos">Vice President, Administration</span><br>
				<a class="readmore" href="/Team/Annie+Piotrowski?cat=Management" title="Read More About Annie Piotrowski">Read More</a>
			</div>
		</div>
	</div>

<?php get_footer(); ?>