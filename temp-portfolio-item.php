<?php /* Template Name: Portfolio Item */ ?>
<?php get_header(); ?>
<div id="mainContent">
	<h1 class="int">Cleversafe Inc.</h1>
	<p class="backOnePage">
		<a href="/Pages/Portfolio" title="Back to Portfolio">Back to Portfolio</a>
	</p>
	<div class="contentBG fullReport clearfix">
		<div class="leftColumn">
			<img src="/assets/root/images/investments/cleversafe_resize160x160.jpg" class="" alt="Cleversafe Inc.">
			<div>Cleversafe Inc., the solution for limitless data storage recently announced breakthrough capabilities for combined storage and massive computation, being the first system to support storage and analysis of datasets at previously unattainable scale with unparalleled reliability and efficiency. &nbsp;Cleversafe’s object based storage system is 100 million times more reliable than traditional RAID-based systems. &nbsp;Its information dispersal capabilities reduce storage costs up to 90 percent while meeting compliance requirements and ensuring protection against data loss. &nbsp;Cleversafe’s customers include many major enterprise, government, medical, media, and other companies facing the most challenging big data issues at petabyte levels and beyond. &nbsp;Its investors include several of the top government and venture capital firms in the United States.</div><div><br></div><div>Comparable companies have recently sold for between $1 billion to $3 billion. &nbsp;Cleversafe also has a potentially highly valuable patent portfolio having filed to date over 700 patents worldwide. &nbsp;Jonathan Zakin and Len Batterson sourced the investment, brought in many high net worth accredited investors at an important early development juncture, and Jonathan served as company CEO for a critical period in it’s early development.</div>
		</div>
		<div class="rightColumn">
			<ul>
				<li><strong>Industry:</strong> Technology</li>
				<li><strong>Location:</strong> Chicago, Illinois</li>
				<li class="url">
					<a href="http://www.cleversafe.com" target="_blank" title="Visit Cleversafe Inc.'s website">www.cleversafe.com</a>
				</li>
				<li></li>
			</ul>
		</div>
	</div>
<?php get_footer(); ?>