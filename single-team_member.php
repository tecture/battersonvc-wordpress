<?php /* Template Name: Team Member */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div id="mainContent">
	<h1 class="int"><?php the_title(); ?></h1>
	<p class="backOnePage"><a href="/Team" title="Back to Portfolio">Back to Team</a></p>

	<div class="contentBG clearfix" id="bioLanding">
		<div class="leftColumn">
			<?php if(get_field('bio_picture')) { ?>
				<img class="bioImage" src="<?php $image = get_field('bio_picture'); echo $image['url']; ?>" alt="bio picture">
			<?php } ?>
			<h2><?php the_field("position"); ?></h2>
			<div class="fullBio">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="rightColumn">
			<?php if(in_category(2)) { ?> 
				<h3 class="moduleHeader">Management</h3>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'team_member',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'category__in' => array( 2 )
		    	);

				// the query
				$the_query = new WP_Query( $args2 ); ?>

				<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post();
				$image = get_field("bio_picture"); ?>
					<div class="md clearfix">
						<a class="linkImg" href="<?php the_permalink(); ?>" title="Read More About <?php the_title(); ?>">
							<img width="116" height="116" src="<?php echo $image['url']; ?>" alt="<?php the_title(); ?> bio picture">
							<span class="view">view</span>
						</a>
						<strong><?php the_title(); ?></strong>
						<span class="pos"><?php the_field('position'); ?></span>
						<br>
						<a class="readmore" href="<?php the_permalink(); ?>" title="Read More About <?php the_title(); ?>">Read More</a>
					</div>
				<?php endwhile; ?>
				<?php endif; ?>      
				<?php wp_reset_postdata();  ?>
			<?php  } else { ?>
				<?php 
		    	$args3 = array(
		    		'post_type' => 'team_member',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'category__in' => array( 3 )
		    	);

				// the query
				$the_query = new WP_Query( $args3 ); ?>

				<?php if ( $the_query->have_posts() ) : ?>
				<h3 class="moduleHeader">Affiliates</h3>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<div class="md clearfix">
						<strong><?php the_title(); ?></strong>
						<a class="readmore" href="<?php the_permalink(); ?>" title="Read More About <?php the_title(); ?>">Read More</a>
					</div>
				<?php endwhile; ?>
				<?php endif; ?>      
				<?php wp_reset_postdata();  
			}  ?>
		</div>
	</div>
	<?php endwhile; ?>
<?php endif; ?>    

<?php get_footer(); ?>