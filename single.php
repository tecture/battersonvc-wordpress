<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
    $date = get_the_date();
    $date = new DateTime($date);
?>
<div id="mainContent">
  <h1 class="int">News</h1>
  <p class="backOnePage">
    <a href="/news" title="Back to News">Back to News</a>
  </p>
  <div class="contentBG clearfix" id="newsLanding">
    <div class="leftColumn">

      <div class="contentContainer">
         <div class="dateContain">
            <div class="month"><?php echo $date->format('M'); ?></div><div class="day"><?php echo $date->format('d'); ?></div><div class="year"><?php echo $date->format('Y'); ?></div>
         </div>
        <div class="newsArticle">
          <h2><?php the_title(); ?></h2>
          <div>
            <?php the_content(); ?>
          </div>
          <br>
          <b>Industry:</b> <?php the_field('industry'); ?>
        </div>
      </div>
    </div>

    <div class="rightColumn">
      <div class="module module-111">
        <h3>Recent News by Year</h3>
        <?php 
          $args2 = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
                'order' => 'ASC',
                'date_query' => array(
                array(
                    'year'  => 2013,
                ),
            ),
          );
        // the query
        $the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
        <div>
          <h2 class="heading">
            <span class="toggleArrow"></span>
            2013
          </h2> 
          <div class="contentToggle" style="display: none;">         
              <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date); ?>
            <div class="articleRightTitle">
                    <a href="<?php the_permalink(); ?>">
                        <h3>
                          <span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
                          <?php the_title(); ?>
                        </h3>  
                    </a>
                </div>
            <?php endwhile; ?>
            </div>   
          </div>     
        </div>
        <?php endif; wp_reset_postdata(); ?>
        <?php 
          $args2 = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
                'order' => 'ASC',
                'date_query' => array(
                array(
                    'year'  => 2016,
                ),
            ),
          );
        // the query
        $the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
        <div>
          <h2 class="heading">
            <span class="toggleArrow"></span>
            2016
          </h2> 
          <div class="contentToggle" style="display: none;">         
              <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date); ?>
            <div class="articleRightTitle">
                    <a href="<?php the_permalink(); ?>">
                        <h3>
                          <span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
                          <?php the_title(); ?>
                        </h3>  
                    </a>
                </div>
            <?php endwhile; ?>
            </div>   
          </div>     
        </div>
        <?php endif; wp_reset_postdata(); ?>
        <?php 
          $args2 = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
                'order' => 'ASC',
                'date_query' => array(
                array(
                    'year'  => 2018,
                ),
            ),
          );
        // the query
        $the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
        <div>
          <h2 class="heading">
            <span class="toggleArrow"></span>
            2018
          </h2> 
          <div class="contentToggle" style="display: none;">         
              <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date); ?>
            <div class="articleRightTitle">
                    <a href="<?php the_permalink(); ?>">
                        <h3>
                          <span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
                          <?php the_title(); ?>
                        </h3>  
                    </a>
                </div>
            <?php endwhile; ?>
            </div>   
          </div>     
        </div>
        <?php endif; wp_reset_postdata(); ?>
        <?php 
          $args2 = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
                'order' => 'ASC',
                'date_query' => array(
                array(
                    'year'  => 2019,
                ),
            ),
          );
        // the query
        $the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
        <div>
          <h2 class="heading">
            <span class="toggleArrow"></span>
            2019
          </h2> 
          <div class="contentToggle" style="display: none;">         
              <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date); ?>
            <div class="articleRightTitle">
                    <a href="<?php the_permalink(); ?>">
                        <h3>
                          <span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
                          <?php the_title(); ?>
                        </h3>  
                    </a>
                </div>
            <?php endwhile; ?>
            </div>   
          </div>     
        </div>
        <?php endif; wp_reset_postdata(); ?>
        <?php 
          $args2 = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
                'order' => 'ASC',
                'date_query' => array(
                array(
                    'year'  => 2020,
                ),
            ),
          );
        // the query
        $the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
        <div>
          <h2 class="heading">
            <span class="toggleArrow"></span>
            2020
          </h2> 
          <div class="contentToggle" style="display: none;">         
              <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date); ?>
            <div class="articleRightTitle">
                    <a href="<?php the_permalink(); ?>">
                        <h3>
                          <span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
                          <?php the_title(); ?>
                        </h3>  
                    </a>
                </div>
            <?php endwhile; ?>
            </div>   
          </div>     
        </div>
        <?php endif; wp_reset_postdata(); ?>
        <?php 
          $args2 = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
                'order' => 'ASC',
                'date_query' => array(
                array(
                    'year'  => 2021,
                ),
            ),
          );
        // the query
        $the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
        <div>
          <h2 class="heading">
            <span class="toggleArrow"></span>
            2021
          </h2> 
          <div class="contentToggle" style="display: none;">         
              <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date); ?>
            <div class="articleRightTitle">
                    <a href="<?php the_permalink(); ?>">
                        <h3>
                          <span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
                          <?php the_title(); ?>
                        </h3>  
                    </a>
                </div>
            <?php endwhile; ?>
            </div>   
          </div>     
        </div>
        <?php endif; wp_reset_postdata(); ?>
        <?php 
          $args2 = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
                'order' => 'ASC',
                'date_query' => array(
                array(
                    'year'  => 2022,
                ),
            ),
          );
        // the query
        $the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
        <div>
          <h2 class="heading">
            <span class="toggleArrow"></span>
            2022
          </h2> 
          <div class="contentToggle" style="display: none;">         
              <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date); ?>
            <div class="articleRightTitle">
                    <a href="<?php the_permalink(); ?>">
                        <h3>
                          <span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
                          <?php the_title(); ?>
                        </h3>  
                    </a>
                </div>
            <?php endwhile; ?>
            </div>   
          </div>     
        </div>
        <?php endif; wp_reset_postdata(); ?>
        <?php 
          $args2 = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
                'order' => 'ASC',
                'date_query' => array(
                array(
                    'year'  => 2023,
                ),
            ),
          );
        // the query
        $the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
        <div>
          <h2 class="heading">
            <span class="toggleArrow"></span>
            2023
          </h2> 
          <div class="contentToggle" style="display: none;">         
              <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date); ?>
            <div class="articleRightTitle">
                    <a href="<?php the_permalink(); ?>">
                        <h3>
                          <span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
                          <?php the_title(); ?>
                        </h3>  
                    </a>
                </div>
            <?php endwhile; ?>
            </div>   
          </div>     
        </div>
        <?php endif; wp_reset_postdata(); ?>
        <?php 
          $args2 = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
                'order' => 'ASC',
                'date_query' => array(
                array(
                    'year'  => 2024,
                ),
            ),
          );
        // the query
        $the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
        <div>
          <h2 class="heading">
            <span class="toggleArrow"></span>
            2018
          </h2> 
          <div class="contentToggle" style="display: none;">         
              <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date); ?>
            <div class="articleRightTitle">
                    <a href="<?php the_permalink(); ?>">
                        <h3>
                          <span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
                          <?php the_title(); ?>
                        </h3>  
                    </a>
                </div>
            <?php endwhile; ?>
            </div>   
          </div>     
        </div>
        <?php endif; wp_reset_postdata(); ?>
        <?php 
          $args2 = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
                'order' => 'ASC',
                'date_query' => array(
                array(
                    'year'  => 2025,
                ),
            ),
          );
        // the query
        $the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
        <div>
          <h2 class="heading">
            <span class="toggleArrow"></span>
            2018
          </h2> 
          <div class="contentToggle" style="display: none;">         
              <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date); ?>
            <div class="articleRightTitle">
                    <a href="<?php the_permalink(); ?>">
                        <h3>
                          <span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
                          <?php the_title(); ?>
                        </h3>  
                    </a>
                </div>
            <?php endwhile; ?>
            </div>   
          </div>     
        </div>
        <?php endif; wp_reset_postdata(); ?>
      </div>
      <div class="module module-22">
        <?php if(get_field("sidebar_quote")) { ?>
        <div>
          <h3>"<?php the_field("sidebar_quote"); ?>"</h3>
        </div>
        <?php } ?>
      </div>
      <div class="module module-112">
        <h3 class="moduleTitle"><?php the_field('sidebar_title'); ?></h3>
        <div>
          <?php the_field('sidebar_content'); ?>
        </div>
        <div>
          <br>
        </div>
        <?php if(get_field('sidebar_link')) { ?>
        <div>
          <a class="go" href="<?php the_field('sidebar_link'); ?>">Submit Plan</a>
        </div>
        <?php } ?>
      </div>


    </div>

  </div>
    <?php endwhile; ?>
<?php endif; ?>   
<?php get_footer(); ?>

