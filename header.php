<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://developers.facebook.com/schema/" xml:lang="en" lang="en">
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149677898-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-149677898-1');
</script>
    
	<title>Batterson Venture Capital, LLC - Batterson Venture Capital</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Batterson Venture Capital, LLC (“BVC”) is a team of highly experienced venture capitalists with more than 100+ years of combined venture capital investing and company building experience." />
	
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/css/bvcStyles.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/fonts/webfonts.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/css/forms.css">
	
	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js" type="text/javascript"></script>-->
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/flexslider/jquery.flexslider.js" type="text/javascript"></script>
	<!--<script src="http://flexslider.woothemes.com/js/jquery.flexslider.js" type="text/javascript"></script>-->
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/js/func.js" type="text/javascript"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-33271265-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>

	
</head>
<body>

	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/jquery-ui.min.js" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/js/ui.main.js?v=2.9.236b" type="text/javascript"></script>

        


<div id="mainContainer">

	<div id="topBar">
		<noscript>For a better experience please enable Javascript.</noscript>
		<ul id="nav">
			<li><a href="/">
				<img alt="Batterson Venture Capital" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/Batterson-Venture-Capital.gif"/></a>
			</li>
				
				<li class='nav-item nav-item-3'><a href="/core-elements/" title="Core Elements">Core Elements</a>
					<ul class="subNav">
							
						
						</ul>
				</li>
				
				<li class='nav-item nav-item-4'><a href="/team" title="Team">Team</a>
					<ul class="subNav">
							
						
						</ul>
				</li>
				
				<li class='nav-item nav-item-5'><a href="/current-investments/" title="Portfolio">Portfolio</a>
					<ul class="subNav">
							
								<li class='sub-nav-item-17' rel='/current-investments/'><a href="/current-investments/" title="Current Investments">Current Investments</a></li>
							
								<li class='sub-nav-item-18' rel='/prior-investments/'><a href="/prior-investments/" title="Prior Investments">Prior Investments</a></li>
							
						
						</ul>
				</li>
				
				<li class='nav-item nav-item-114'><a href="/news" title="News">News</a>
					<ul class="subNav">
							
						
						</ul>
				</li>
				
			<li class='nav-item nav-item-contact'><a href="javascript:;" title="Contact Batterson Venture Capital">Contact</a></li>
		</ul>

		<div class="contactForm">
		<div class="contactContent clearfix">  
		   <div class="contactLeft">
		         <h2>Contact</h2>
		         
			<?php echo do_shortcode('[contact-form-7 id="98" title="Contact form 1"]'); ?>
		    </div>

		   <div class="contactRight">
		         <p>All of our greatest achievements have started with but a conversation. Whether you're an investor interested in exploring our portfolio of new opportunities in greater depth or if you're an entrepreneur or like-minded professional looking for that ideal relationship, taking that step towards a partnership with BVC starts with you. We look forward to hearing from you.</p>
		         <h2>Submit Your Business Plan</h2>
		         <p>We welcome the opportunity to review your business. If you would like to submit an executive summary or business plan, clicking "Submit Plan" below will take you to the appropriate form. Please note that we have a preference for executive summaries as a first contact. If we need to see the plan before deciding on next steps, we will contact you.</p>
		         <a href="/submit-a-business-plan/" class="go">Submit Plan</a>
		   </div>
		<a class="closeContact" href="javascript:;" title="close contact form">close <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/icons/close.gif" /></a>
		</div>

		</div>


	</div>
	<div class="header mobileHeader">
		<ul class="menu">
			<li><a href="/Core-Elements">Core Elements</a></li>
			<li><a href="/Team">Team</a></li>
			<li><a href="/current-investments">Portfolio</a></li>
			<li><a href="/News">News</a></li>
			<li><a href="/Contact">Contact</a></li>
		</ul>
		<a class="menuLink" href="javascript:;">Menu +</a>
		<a href="/">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/mobile/batterson-venture-capital.jpg" alt="Batterson Venture Capital">
		</a>
	</div>
