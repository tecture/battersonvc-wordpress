<?php /* Template Name: Team  */ ?>
<?php get_header(); ?>
<div id="mainContent">
	<h1 class="int">Team</h1>
	<div class="contentBG clearfix">
		<div class="leftColumn">
			<div id="teamLanding">
				<?php 
		    	$args2 = array(
		    		'post_type' => 'team_member',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'category__in' => array( 2 )
		    	);

				// the query
				$the_query = new WP_Query( $args2 ); ?>

				<?php if ( $the_query->have_posts() ) : ?>
					<a name="instance-10">&nbsp;</a>
					<h2><span>Management</span></h2>
					<div class="clearfix mdInfo mdGroup-10">
					<?php while ( $the_query->have_posts() ) : $the_query->the_post();
						$image = get_field("bio_picture");
					?>
						<div class="md">
							<?php if($image) { ?>
								<a href="<?php the_permalink(); ?>" title="Read More About <?php the_title(); ?>">
									<img width="145" height="145" src="<?php echo $image['url']; ?>" alt="<?php the_title(); ?> bio picture">
									<span class="view">view</span>
								</a>
							<?php } ?>

							<strong><?php the_title(); ?></strong>
							<span class="position"><?php the_field("position"); ?></span>
							<br>
							<a class="readmore" href="<?php the_permalink(); ?>" title="Read More About <?php the_title(); ?>">Read More</a>
						</div>

					<?php endwhile; ?>
				</div>
				<?php endif; ?>      
				<?php wp_reset_postdata(); ?>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'team_member',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'category__in' => array( 3 )
		    	);

				// the query
				$the_query = new WP_Query( $args2 ); ?>

				<?php if ( $the_query->have_posts() ) : ?>
					<a name="instance-12">&nbsp;</a>
					<h2><span>Affiliates</span></h2>
					<div class="clearfix mdInfo mdGroup-12">
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<div class="md">

							<strong><?php the_title(); ?></strong>
							<?php if(get_field('position')) { ?>
								<span class="position"><?php the_field("position"); ?></span>
							<?php } ?>
							<div class="description"><?php the_field("snippet"); ?></div>
							<br>
							<a class="readmore" href="<?php the_permalink(); ?>" title="Read More About <?php the_title(); ?>">Read More</a>
						</div>
					<?php endwhile; ?>
					</div>
				<?php endif; ?>      
				<?php wp_reset_postdata();  ?>

				<?php if( have_rows('advisory_director') ) { ?>
					<a name="instance-85">&nbsp;</a>
					<h2><span>Advisory Directors</span></h2>
					<div class="clearfix mdInfo mdGroup-85">
						<?php while ( have_rows('advisory_director') ) : the_row(); ?>
							<div class="md">
								<strong><?php the_sub_field("name"); ?></strong>
								<span class="position"><?php the_sub_field("position"); ?></span>
							</div>
						<?php endwhile; ?>
					</div>
				<?php } ?>      
			</div>

			<div class="counter"></div>
			<div class="contentContainer"><br></div>
		</div>
		<div class="rightColumn">
			<div class="module module-22">
				<div>
					<h3>"<?php the_field("sidebar_qoute"); ?>"</h3>
				</div>
			</div>
			<div class="module module-43">
				<ul class="teamNav" style="position: static; margin-top: 0px;">
					<li><a href="#instance-10">Management</a></li>
					<li><a href="#instance-12">Affiliates</a></li>
					<li><a href="#instance-85">Advisory Directors</a></li>
				</ul>
				<script type="text/javascript">
					$(document).scroll(function() {
					if($(document).scrollTop() > 360){ $("ul.teamNav").css({"position":"fixed","margin-top":"-360px"}); } else { $("ul.teamNav").css({"position":"static","margin-top":"0"}); }
					})
				</script>
			</div>
		</div>
	</div>

<?php get_footer(); ?>