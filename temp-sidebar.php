<?php /* Template Name: Page w/ Sidebar */ ?>
<?php get_header(); ?>
<div id="mainContent">
	<h1 class="int"><?php the_title(); ?></h1>
	<div class="contentBG clearfix">
		<div class="leftColumn">
			<!--<img src="/assets/root/images/placements/investment-criteria.jpg" alt="Investment Criteria" style="max-width:642px;">-->
			<div class="contentContainer">
				<?php the_content(); ?> 
			</div>
		</div>
		<div class="rightColumn">
			<div class="module module-22">
				<div>
					<h3>"It is from those with imagination that our world's growth and possibilities become our truths."</h3>
				</div>
			</div>
			<div class="module module-23">
				<h3 class="moduleTitle">About Len</h3>
				<div>
					<a href="/Team/Len-Batterson?cat=Managing Directors">
						<img src="/assets/root/images/staff/len03_resize145x145.jpg" alt="Len Batterson">
					</a>
					Len Batterson is Chairman and CEO of Batterson Venture Capital, LLC. As a pioneering venture capitalist and entrepreneur with an annual IRR of 29% over the past 27 years, Len has invested in and founded many venture-backed entrepreneurial companies and venture capital investment organizations.
					<div>
						<br>
					</div>
				</div>
				<div>
					<a class="readmore" href="/Team/Len-Batterson?cat=Managing Directors">Read More</a>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>