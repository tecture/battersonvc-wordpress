<?php /* Template Name: Core Elements */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div id="mainContent">
	<h1 class="int">Core Elements</h1>
	<div class="contentBG clearfix">
		<div class="leftColumn">
			<img src="<?php $image = get_field('banner_image'); echo $image['url']; ?>" alt="Investment Criteria" style="max-width:100%;">

			<!--
			<ul class="investCrit">
				<li><img src="/assets/root/images/icons/bvc-understand.gif" alt="Understanding Available Opportunities" /><h3>Understanding Available Opportunities</h3></li>
				<li><img src="/assets/root/images/icons/batterson-research.gif" alt="Researching Current Markets and Competition" /><h3>Researching Current Markets and Competition</h3></li>
				<li><img src="/assets/root/images/icons/batterson-establish.gif" alt="Establishing Adaptable Business Designs" /><h3>Establishing Adaptable Business Designs</h3></li>
				<li><img src="/assets/root/images/icons/batterson-manage.gif" alt="Managing Effectively Towards Success" /><h3>Managing Effectively Towards Success</h3></li>
			</ul>
			-->
			<div class="contentContainer">
				<?php if(have_rows('expand') ): ?>
					<?php while(have_rows('expand')): the_row(); ?>
						<h2 class="heading">
							<span class="toggleArrow"></span>
							<font size="5"><?php the_sub_field('title'); ?></font>
						</h2> 
						<div class="contentToggle" style="display: none;">
							<?php the_sub_field('details'); ?>
						</div>  
					<?php endwhile; ?>
				<?php endif; ?>   
			</div>
		</div>
		<div class="rightColumn">
			<div class="module module-22">
				<div>
					<h3>"<?php the_field("sidebar_quote"); ?>"</h3>
				</div>
			</div>
			<div class="module module-23">
				<h3 class="moduleTitle"><?php the_field('sidebar_title'); ?></h3>
				<div>
					<?php the_field('sidebar_content'); ?>
					<!--<a href="/Team/Len+Batterson?cat=Managing Directors">
						<img src="/assets/root/images/staff/len03_resize145x145.jpg" alt="Len Batterson">
					</a>
					Len Batterson is Chairman and CEO of Batterson Venture Capital, LLC. As a pioneering venture capitalist and entrepreneur with an annual IRR of 29% over the past 27 years, Len has invested in and founded many venture-backed entrepreneurial companies and venture capital investment organizations.
					<div>
						<br>
					</div>-->
				</div>
				<div>
					<br>
				</div>
				<div>
					<a class="readmore" href="<?php the_field('sidebar_link'); ?>">Read More</a>
				</div>
			</div>
		</div>
	</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>