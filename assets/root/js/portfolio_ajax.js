$(document).ready(function(){

$(".rightColumn").attr('id', 'portLanding');
xmlURL =  "/xml.ashx?name="+xmlName;
    $.ajax({
        type: "GET",
        url: xmlURL,
        dataType: "xml",
async: false,
        success: function(xml) {
var xmlHTML = "";
var xmlHTML2 = "";
var xmlID = 0;

$(xml).find('investment').each(function(){



xmlID++;
xmlHTML += "<a href=\"\" rel=\"" + $(this).attr("name").replace(/ /g,'') + "\" class=\"portImg";
if(xmlID % 4 === 0){xmlHTML += " lastInRow"; }
if(parseInt(xmlID - 1) % 4 === 0 || xmlID === 1){xmlHTML += " firstInRow"; }
xmlHTML += "\"  title=\"View " + $(this).find('title').text() + "\"><span class=\"portBoxer bwFade\"><img src=\"/" + $(this).find('imageBig').text() + "\" alt=\"" + $(this).find('title').text() + "\" /></span></a>";

//console.log($(this).find('fullReport').text());

xmlHTML2 += "<div class=\"portInfo\" style=\"display:none;\" id=\"" + $(this).attr("name").replace(/ /g,'') + "\">";
xmlHTML2 += "<a href=\"http://" + $(this).find('url').text() + "\" target=\"_blank\" title=\"Visit " + $(this).find('title').text() + "'s website\"><img src=\"/" + $(this).find('imageThumb').text() + "\" class=\"portThumb\" alt=\"" + $(this).find('title').text() + "\"></a>";
xmlHTML2 += "<div class=\"info clearfix\"><h3 class=\"moduleTitle\">" + $(this).find('title').text() + "</h3><ul>";
xmlHTML2 += ($(this).find('industry').text() != '' ? "<li>Industry: " + $(this).find('industry').text() + "</li><li>" + $(this).find('location').text() + "</li>" : "");
xmlHTML2 += "<li class=\"url\"><a href=\"http://" + $(this).find('url').text() + "\" target=\"_blank\" title=\"Visit " + $(this).find('title').text() + "'s website\">" + $(this).find('url').text() + "</a></li><li>";
xmlHTML2 += "</li></ul></div><div class=\"fullReport\">" + $(this).find('synopsis').text() + "</div>";
if($(this).find('fullReport').text().length > 0){
xmlHTML2 += "<div class=\"fullReport\"><a href=\"/" + $(this).find('urlName').text() + "\" title=\"View " + $(this).find('title').text() + "'s full report\">View Full Report</a></div>";
}
xmlHTML2 += "</div>";


});

$("#port").html(xmlHTML);
$(".rightColumn").append(xmlHTML2);
$('.hoverizrImg').hoverizr();
        }
    });
});


$("#port a.portImg").live("click", function(){
	var windowWidth = $(window).width();
	var link = $(this).attr("data-link");

	console.log(link);

	if(windowWidth < 950) {
		window.location.replace(link);
		return false
	} else {
		var myportId = $(this).attr("rel");
		console.log('fired ' + myportId);
		$("div.module, div.portInfo").hide();
		$(".contentBG").css({"background-image":"url(/wp-content/themes/batterson/assets/root/images/backgrounds/rightColumn-white.gif)"});
		$("#" + myportId).fadeIn();
		return false
	}
});


    

    

	// On window load. This waits until images have loaded which is essential
	$(window).load(function(){
		
		// Fade in images so there isn't a color "pop" document load and then on window load
		$(".bwFade .img_grayscale").fadeIn(500);
		
		// clone image
		/*$('.bwFade img').each(function(){
			var el = $(this);
			el.css({"position":"absolute"}).wrap("<div class='img_wrapper' style='display:inline-block'>").clone().addClass('img_grayscale').css({"position":"absolute","z-index":"990","opacity":"0"}).insertBefore(el).queue(function(){
				var el = $(this);
				el.parent().css({"width":this.width,"height":this.height});
				el.dequeue();
			});
			this.src = grayscale(this.src);
		});*/


		
		// Fade image 
		$('.portImg').mouseover(function(){ 
			$(this).find(".img_color").stop().animate({opacity:1}, 1000);

		});
		$('.portImg').mouseleave(function(){ 
			$(this).find(".img_color").stop().animate({opacity:0}, 1000);
		});		
	});
	
	// Grayscale w canvas method
	function grayscale(src){
		var canvas = document.createElement('canvas');
		var ctx = canvas.getContext('2d');
		var imgObj = new Image();
		imgObj.src = src;
		canvas.width = imgObj.width;
		canvas.height = imgObj.height; 
		ctx.drawImage(imgObj, 0, 0); 
		var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
		for(var y = 0; y < imgPixels.height; y++){
			for(var x = 0; x < imgPixels.width; x++){
				var i = (y * 4) * imgPixels.width + x * 4;
				var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
				imgPixels.data[i] = avg; 
				imgPixels.data[i + 1] = avg; 
				imgPixels.data[i + 2] = avg;
			}
		}
		ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
		return canvas.toDataURL();
    }
    

    

    

    

	$(window).ready(function(){

		$("a.portImg")
		
		$("a.portImg").live("mouseover",function(){
		$(this).find(".viewlink").show();
		$(this).prepend("<span class=\"portFrame\"></span>");
		$(this).prepend("<span class=\"viewlinks\">synopsis</span>");
		});
		$("a.portImg").live("mouseleave",function(){
		$(this).find(".viewlinks").remove();
		$(this).find(".portFrame").remove();
		});
		
		
		
		if(window.location.hash) {
		var thisHashTag = window.location.hash;
		$("div.module, div.portInfo").hide();
		$(".contentBG").css({"background-image":"url(/assets/root/images/backgrounds/rightColumn-white.gif)"});
		$(thisHashTag.replace(/ /g,'')).fadeIn();
		}
		
		
		});