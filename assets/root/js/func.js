/* JavaScript Document
	Author: RR
	Date: 7/11/2011

*/

//Gladiator Ready?!? Document Ready?!?
$(function() {
    if ($('.contactForm .wpcf7-validation-errors').length) {
        setTimeout(function() { // This wasn't working right away.
            $('.nav-item-contact a').click();
        }, 500)
    }

  $(".nav-item-5 a:first").attr("href", "/Pages/Current-Investments");

  $(".nav-item-5").mouseenter(function() {
    $(".nav-item-5 .subNav", this).css("display", "block");
  });
  $(".nav-item-5").mouseleave(function() {
    $(".nav-item-5 .subNav", this)
      .delay(800)
      .css("display", "none");
  });

  var alertFallback = true;
  if (typeof console === "undefined" || typeof console.log === "undefined") {
    console = {};
    if (alertFallback) {
      console.log = function(msg) {
        alert(msg);
      };
    } else {
      console.log = function() {};
    }
  }

  // Contact Form expansion
  try {
    $(".contactForm").hide();

    $(".nav-item-contact").click(function() {
      $(".contactForm").slideToggle();
      $(".contactForm").toggleClass("expand");
    });

    $(".closeContact").click(function() {
      $(".contactForm").slideToggle();
      $(".contactForm").toggleClass("expand");
    });
  } catch (err) {}

  // Portfolio Hover
  try {
    $(".nav-item").mouseover(function() {
      $(this)
        .children("ul.subNav")
        .show();
    });
    $(".nav-item").mouseout(function() {
      $(this)
        .children("ul.subNav")
        .hide();
    });
  } catch (err) {}

  // every 4th portfolio Image gets a class! You go Glen Coco!
  try {
    $(".portImg:nth-child(4n)").addClass("lastInRow");
  } catch (err) {}

  // Pager! You've been bad! Cleario-fixeo!
  try {
    $(".pager").prepend('<div class="clearfix">&nbsp;</div>');
  } catch (err) {}

  // Search for tab navigation add a class
  try {
    if ($(".tabNav").length != 0) $(".contentBG").addClass("tabsAdded");
  } catch (err) {}

  try {
    $(".md a").mouseover(function() {
      $(this)
        .find(".view")
        .show();
    });
    $(".md a").mouseleave(function() {
      $(this)
        .find(".view")
        .hide();
    });
  } catch (err) {}
});

$(document).ready(function() {
  $(".contentToggle").hide();

  $(".heading").click(function() {
    $(this)
      .find("span")
      .toggleClass("toggleArrowDown");
    $(this)
      .next(".contentToggle")
      .slideToggle();
    return false;
  });

  $('.menuLink').click(function(){
        $('ul.menu').slideToggle();
    });
});

$(function() {
  if ($(window).width() < 951) {
    $("body").scrollTop(1);

    var alertFallback = true;
    if (typeof console === "undefined" || typeof console.log === "undefined") {
      console = {};
      if (alertFallback) {
        console.log = function(msg) {
          alert(msg);
        };
      } else {
        console.log = function() {};
      }
    }

    try {
      $(".menuLink").click(function() {
        $("ul.menu").toggleClass('expanded');
      });
    } catch (err) {}

    $(window).load(function() {
      $(".flexslider").flexslider({
        animation: "slide",
        controlNav: "thumbnails"
      });
    });
  }
});
