<?php /* Template Name: Current Investments */ ?>
<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="mainContent">
	<h1 class="int"><?php the_title(); ?></h1>
	<div class="contentBG clearfix tabsAdded">
		<div class="leftColumn">
			<ul class="tabNav">
				<li class="menu-item menu-item-17">
					<a href="/current-investments/" title="Current Investments" class="selected">Current Investments</a>
				</li>
				<li class="menu-item menu-item-18">
					<a href="/prior-investments/" title="Prior Investments">Prior Investments</a>
				</li>
			</ul>
			<script type="text/javascript"> var xmlName = "Portfolio"; </script>
			<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/js/portfolio_ajax.js" type="text/javascript"></script>
			<?php 
	    	$args1 = array(
	    		'post_type' => 'investment',
	    		'posts_per_page' => -1,
	            'order' => 'DESC',
	            'category__in' => array( 4 )
	    	);

			// the query
			$the_query = new WP_Query( $args1 ); ?>

			<?php if ( $the_query->have_posts() ) : ?>
			<div class="row" id="port">
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php 
					$id2 = get_the_title();
					$id2 = str_replace(' ', '', $id2);
					$id2 = str_replace('.', '', $id2);
					$id2 = str_replace(',', '', $id2);
					?>
					<a href="" rel="<?php echo $id2; ?>" class="portImg firstInRow" data-link="<?php the_permalink(); ?>" title="View <?php the_title(); ?>">
						<span class="portBoxer bwFade">
							<div class="img_wrapper" style="display: inline-block; width: 160px; height: 160px;">
								<img src="<?php $image = get_field('color_image'); echo $image['url']; ?>" alt=" <?php the_title(); ?>" style="opacity: 0; display: inline; position: absolute; z-index: 990;" class="img_color">
								<img src="<?php $black_and_white_image = get_field('black_and_white_image'); echo $black_and_white_image['url']; ?>" alt=" <?php the_title(); ?>" style="display: inline; position: absolute; opacity: 1;" class="img_grayscale">
							</div>
						</span>
					</a>			
				<?php endwhile; ?>
			</div>
			<?php endif; wp_reset_postdata(); ?>
			<?php 

				// the query
				$the_query = new WP_Query( $args1 ); ?>

				<?php if ( $the_query->have_posts() ) : ?>
				<noscript>
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<a href="<?php the_permalink(); ?>" rel="" class="" title="View <?php the_title(); ?>">
							<span class="portBoxer bwFade">
								<img width="160" height="160" src="<?php $image = get_field('color_image'); echo $image['url']; ?>" alt="<?php the_title(); ?>" />
							</span>
						</a>
					<?php endwhile; ?>
				</noscript>
			<?php endif; wp_reset_postdata(); ?>
			<div class="contentContainer">
				<br>
			</div>
		</div>
		<div class="rightColumn" id="portLanding">
			<div class="module module-22">		
				<div>
					<h3>"<?php the_field("sidebar_quote"); ?>"</h3>
				</div>
			</div>
			<div class="module module-23">
				<h3 class="moduleTitle"><?php the_field("sidebar_title"); ?></h3>
				<div>
					<?php the_field('sidebar_content'); ?>
					<div>
						<br>
					</div>
				</div>
				<div>
					<a class="readmore" href="<?php the_field('sidebar_link'); ?>">Read More</a>
				</div>
			</div>
			<?php
			// the query
			$the_query = new WP_Query( $args1 ); ?>

			<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php 
					$id = get_the_title();
					$id = str_replace(' ', '', $id);
					$id = str_replace('.', '', $id);
					$id = str_replace(',', '', $id);
				?>
				<div class="portInfo" style="display:none;" id="<?php echo $id; ?>">
					<a href="<?php the_field('site_url'); ?>" target="_blank" title="Visit  <?php the_title(); ?>'s website">
						<img width="90" height="90" src="<?php $image = get_field('color_image'); echo $image['url']; ?>" class="portThumb" alt=" <?php the_title(); ?>">
					</a>
					<div class="info clearfix">
						<h3 class="moduleTitle"> <?php the_title(); ?></h3>
						<ul>
							<li>Industry: <?php the_field('industry'); ?></li>
							<li><?php the_field('location'); ?></li>
							<li class="url">
								<a href="<?php the_field('site_url'); ?>" target="_blank" title="Visit  <?php the_title(); ?>'s website"><?php the_field('site_label'); ?></a>
							</li>
							<li></li>
						</ul>
					</div>
					<div class="fullReport">
						<?php the_field('summary'); ?>
					</div>
					<div class="fullReport">
						<a href="<?php the_permalink(); ?>" title="View <?php the_title(); ?>'s full report">View Full Report</a>
					</div>
				</div>
				<?php 
				endwhile; 
			endif; wp_reset_postdata();
			?>
			</div>
		</div>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/js/jquery.hoverizr.min.js"></script>

<script>

$(window).load(function() {
	 $('.hoverizrImg').hoverizr();
	});

// Add selected tab navigation
try{
$('.tabNav li').find(":contains('Portfolio')").addClass('selected');
}catch(err){}

</script>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>