<?php /* Template Name: News Page  */ ?>
<?php get_header(); ?>
<div id="mainContent">
	<h1 class="int">News</h1>
	<div class="contentBG clearfix">
		<div class="leftColumn">
			<div class="contentContainer">
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'DESC'
		    	);

				// the query
				$the_query = new WP_Query( $args2 ); ?>

				<?php if ( $the_query->have_posts() ) : ?>
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
						$date = get_the_date();
						// make date object
						$date = new DateTime($date);
					?>


			    <div class="newsEntry">
			    	<a href="/News/RIP+Jim+Kimsey" title="View RIP Jim Kimsey">
			           <div class="dateContain">
			               <div class="month"><?php echo $date->format('M'); ?></div>
			               <div class="day"><?php echo $date->format('d'); ?></div>
			               <div class="year"><?php echo $date->format('Y'); ?></div>
			           </div>
			        </a>
			        <div class="newsArticle">
						<a href="<?php the_permalink(); ?>" title="View <?php the_title(); ?>">
							<h2><?php the_title(); ?></h2>
							<div class="mobileDateContain"><?php echo $date->format('F d, Y'); ?></div>
						</a>            
			            <a href="<?php the_permalink(); ?>" title="View <?php the_title(); ?>"></a>
			            <div>
			            	<p></p>
			            	<div style="color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;"><?php the_excerpt(); ?></div>
			            	<p></p>
							<br>
							<b>Industry:</b> <?php the_field('industry'); ?>
							<br>
						</div>
			        </div>
			    </div>   
			    <?php endwhile; ?>
				<?php endif; ?>      
				<?php wp_reset_postdata(); ?>
			</div>
			<div class="contentContainer"></div>
		</div>
		<div class="rightColumn">
			<div class="module module-111">
				<h3>Recent News by Year</h3>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'date_query' => array(
				        array(
				            'year'  => 2013,
				        ),
				    ),
		    	);
				// the query
				$the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
				<div>
					<h2 class="heading">
						<span class="toggleArrow"></span>
						2013
					</h2> 
					<div class="contentToggle" style="display: none;">         
				     	<div>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date);	?>
						<div class="articleRightTitle">
						        <a href="<?php the_permalink(); ?>">
						            <h3>
						            	<span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
						            	<?php the_title(); ?>
						            </h3>  
						        </a>
						    </div>
						<?php endwhile; ?>
						</div>   
					</div>     
				</div>
				<?php endif; wp_reset_postdata(); ?>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'date_query' => array(
				        array(
				            'year'  => 2016,
				        ),
				    ),
		    	);
				// the query
				$the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
				<div>
					<h2 class="heading">
						<span class="toggleArrow"></span>
						2016
					</h2> 
					<div class="contentToggle" style="display: none;">         
				     	<div>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date);	?>
						<div class="articleRightTitle">
						        <a href="<?php the_permalink(); ?>">
						            <h3>
						            	<span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
						            	<?php the_title(); ?>
						            </h3>  
						        </a>
						    </div>
						<?php endwhile; ?>
						</div>   
					</div>     
				</div>
				<?php endif; wp_reset_postdata(); ?>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'date_query' => array(
				        array(
				            'year'  => 2018,
				        ),
				    ),
		    	);
				// the query
				$the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
				<div>
					<h2 class="heading">
						<span class="toggleArrow"></span>
						2018
					</h2> 
					<div class="contentToggle" style="display: none;">         
				     	<div>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date);	?>
						<div class="articleRightTitle">
						        <a href="<?php the_permalink(); ?>">
						            <h3>
						            	<span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
						            	<?php the_title(); ?>
						            </h3>  
						        </a>
						    </div>
						<?php endwhile; ?>
						</div>   
					</div>     
				</div>
				<?php endif; wp_reset_postdata(); ?>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'date_query' => array(
				        array(
				            'year'  => 2019,
				        ),
				    ),
		    	);
				// the query
				$the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
				<div>
					<h2 class="heading">
						<span class="toggleArrow"></span>
						2019
					</h2> 
					<div class="contentToggle" style="display: none;">         
				     	<div>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date);	?>
						<div class="articleRightTitle">
						        <a href="<?php the_permalink(); ?>">
						            <h3>
						            	<span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
						            	<?php the_title(); ?>
						            </h3>  
						        </a>
						    </div>
						<?php endwhile; ?>
						</div>   
					</div>     
				</div>
				<?php endif; wp_reset_postdata(); ?>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'date_query' => array(
				        array(
				            'year'  => 2020,
				        ),
				    ),
		    	);
				// the query
				$the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
				<div>
					<h2 class="heading">
						<span class="toggleArrow"></span>
						2020
					</h2> 
					<div class="contentToggle" style="display: none;">         
				     	<div>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date);	?>
						<div class="articleRightTitle">
						        <a href="<?php the_permalink(); ?>">
						            <h3>
						            	<span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
						            	<?php the_title(); ?>
						            </h3>  
						        </a>
						    </div>
						<?php endwhile; ?>
						</div>   
					</div>     
				</div>
				<?php endif; wp_reset_postdata(); ?>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'date_query' => array(
				        array(
				            'year'  => 2021,
				        ),
				    ),
		    	);
				// the query
				$the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
				<div>
					<h2 class="heading">
						<span class="toggleArrow"></span>
						2021
					</h2> 
					<div class="contentToggle" style="display: none;">         
				     	<div>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date);	?>
						<div class="articleRightTitle">
						        <a href="<?php the_permalink(); ?>">
						            <h3>
						            	<span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
						            	<?php the_title(); ?>
						            </h3>  
						        </a>
						    </div>
						<?php endwhile; ?>
						</div>   
					</div>     
				</div>
				<?php endif; wp_reset_postdata(); ?>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'date_query' => array(
				        array(
				            'year'  => 2022,
				        ),
				    ),
		    	);
				// the query
				$the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
				<div>
					<h2 class="heading">
						<span class="toggleArrow"></span>
						2022
					</h2> 
					<div class="contentToggle" style="display: none;">         
				     	<div>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date);	?>
						<div class="articleRightTitle">
						        <a href="<?php the_permalink(); ?>">
						            <h3>
						            	<span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
						            	<?php the_title(); ?>
						            </h3>  
						        </a>
						    </div>
						<?php endwhile; ?>
						</div>   
					</div>     
				</div>
				<?php endif; wp_reset_postdata(); ?>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'date_query' => array(
				        array(
				            'year'  => 2023,
				        ),
				    ),
		    	);
				// the query
				$the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
				<div>
					<h2 class="heading">
						<span class="toggleArrow"></span>
						2023
					</h2> 
					<div class="contentToggle" style="display: none;">         
				     	<div>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date);	?>
						<div class="articleRightTitle">
						        <a href="<?php the_permalink(); ?>">
						            <h3>
						            	<span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
						            	<?php the_title(); ?>
						            </h3>  
						        </a>
						    </div>
						<?php endwhile; ?>
						</div>   
					</div>     
				</div>
				<?php endif; wp_reset_postdata(); ?>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'date_query' => array(
				        array(
				            'year'  => 2024,
				        ),
				    ),
		    	);
				// the query
				$the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
				<div>
					<h2 class="heading">
						<span class="toggleArrow"></span>
						2018
					</h2> 
					<div class="contentToggle" style="display: none;">         
				     	<div>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date);	?>
						<div class="articleRightTitle">
						        <a href="<?php the_permalink(); ?>">
						            <h3>
						            	<span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
						            	<?php the_title(); ?>
						            </h3>  
						        </a>
						    </div>
						<?php endwhile; ?>
						</div>   
					</div>     
				</div>
				<?php endif; wp_reset_postdata(); ?>
				<?php 
		    	$args2 = array(
		    		'post_type' => 'post',
		    		'posts_per_page' => -1,
		            'order' => 'ASC',
		            'date_query' => array(
				        array(
				            'year'  => 2025,
				        ),
				    ),
		    	);
				// the query
				$the_query = new WP_Query( $args2 );if ( $the_query->have_posts() ) : ?>
				<div>
					<h2 class="heading">
						<span class="toggleArrow"></span>
						2018
					</h2> 
					<div class="contentToggle" style="display: none;">         
				     	<div>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $date = get_the_date(); $date = new DateTime($date);	?>
						<div class="articleRightTitle">
						        <a href="<?php the_permalink(); ?>">
						            <h3>
						            	<span class="rightDate"><?php echo $date->format('m.d.y'); ?></span> 
						            	<?php the_title(); ?>
						            </h3>  
						        </a>
						    </div>
						<?php endwhile; ?>
						</div>   
					</div>     
				</div>
				<?php endif; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>