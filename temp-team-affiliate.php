<?php /* Template Name: Team Member Affiliate */ ?>
<?php get_header(); ?>
<div id="mainContent">
	<h1 class="int">Jonathan Zakin</h1>
	<p class="backOnePage">
		<a href="/Pages/Team" title="Back to Portfolio">Back to Team</a>
	</p>
	<div class="contentBG clearfix" id="bioLanding">
		<div class="leftColumn">
			<div class="fullBio">
				Jonathan is currently active in the entertainment industry as owner of Leeward Productions, L.L.C. that is developing three film properties, and as a Partner in Deutsch-Zakin Media, L.L.C. (“DZM”), owners of the non-book rights to the popular Encyclopedia Brown children’s series.  DZM is currently developing the Encyclopedia Brown franchise into a major live action film, a series of graphic novels, as well as educational software and an electronic game.  Jonathan also owns Seaview Holdings, L.L.C., an investment entity that actively invests in early stage companies.<div><br></div><div>Prior to this he served as Proxim Corporation’s (NASDQ: PROX) Chief Executive Officer and Chairman of the Board of Directors.   Proxim was created by the merger of Western Multiplex and Proxim Inc.   Jonathan, who had been an industrial partner (executive in residence) with Ripplewood Holdings L.L.C., had bought Western Multiplex as a platform to roll up assets in the wireless infrastructure space.  After arranging the Proxim merger and becoming CEO of the combined company, he acquired the wireless assets from Agere (formerly Lucent).  He left Proxim in May 2003, when Warburg Pincus, who financed the Agere purchase, brought in their own CEO. During his tenure the Company grew from approximately $50 million to about $170 million in a very difficult communications market.&nbsp;</div><div><br></div><div>Previously, Jonathan spent 10 years with U.S. Robotics, Inc., a leader in data communications equipment in various management positions including Executive Vice President of Business Development and Corporate Strategy involved in acquisitions (including Palm, Megahertz, Scorpio, etc.) and strategic relationships, Executive Vice President of Sales and Marketing, and Vice President of Sales.  He was a member of the U.S. Robotics Board of Directors for nine years.   Jonathan joined the Company when revenue was $18 million with 150 employees and remained until it was sold and its run rate was approximately $3 billion with 6000 employees.  He was instrumental in engineering the sale of USR to 3Com for over $8 billion, which at the time was the largest sale in the history of the data communications industry.</div>
				<div><br></div>
				<div>Prior to USR, he held various executive and management positions, both domestically and internationally, with Winterhalter, Inc., Cosma International (located in Belgium), Brisk and Kindle (located in London), and J. Henry Schroder Corp. where he worked in the project finance group.  Jonathan is on the board of the UCLA School of Theater, Film and Television and other corporate boards.  Jonathan received an M.B.A. degree from Harvard Business School, with distinction, and a B.S. degree cum laude from New York University.</div>
			</div>
		</div>
		<div class="rightColumn">
			<h3 class="moduleHeader">Affiliates</h3>
			<div class="md clearfix">
				<strong>Jonathan Zakin</strong>
				<a class="readmore" href="/Team/Jonathan+Zakin?cat=Affiliates" title="Read More About Jonathan Zakin">Read More</a>
			</div>
			<div class="md clearfix">
				<strong>Peter Krivkovich</strong>
				<span class="pos">CEO, Co-founder AdRoll</span><br>
				<a class="readmore" href="/Team/Peter+Krivkovich?cat=Affiliates" title="Read More About Peter Krivkovich">Read More</a>
			</div>
		</div>
	</div>

<?php get_footer(); ?>