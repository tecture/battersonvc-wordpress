<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div id="mainContent">
	<h1 class="int"><?php the_title(); ?></h1>
	<p class="backOnePage">
		<a href="/Pages/Portfolio" title="Back to Portfolio">Back to Portfolio</a>
	</p>
	<div class="contentBG fullReport clearfix">
		<div class="leftColumn">
			<img width="160" height="160" src="<?php $image = get_field('color_image'); echo $image['url']; ?>" class="" alt="<?php the_title(); ?>">
			<div><?php the_content(); ?></div>
		</div>
		<div class="rightColumn">
			<ul>
				<li><strong>Industry:</strong> <?php the_field('industry'); ?></li>
				<li><strong>Location:</strong> <?php the_field('location'); ?></li>
				<li class="url">
					<a href="<?php the_field('site_url'); ?>" target="_blank" title="Visit <?php the_title(); ?>'s website"><?php the_field('site_label'); ?></a>
				</li>
				<li></li>
			</ul>
		</div>
	</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>