	<div class="mobilefooter">
		<ul>

		<li><a href="/core-elements/" title="Core Elements">Core Elements</a></li>

		<li><a href="/team" title="Team">Team</a></li>

		<li><a href="/current-investments/" title="Portfolio">Portfolio</a></li>

		<li><a href="/news" title="News">News</a></li>

		<li><a href="/contact-us" title="Contact">Contact</a></li>

		</ul>

		<p>Batterson Venture Capital  •  2018 © Copyright  •  All Rights Reserved</p>
		<a href="http://www.tecture.com" title="Chicago Mobile Design: Tecture"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/mobile/built-by-tecture.gif" alt="Chicago Mobile Design: Tecture"></a>
	</div>
	<div id="footer" class="clearfix">
		<ul>
			<li class='footer-nav-item footer-nav-item-3'>
				<a href="/core-elements/" title="Core Elements - Batterson Venture Capital">Core Elements</a>	
			</li>
			<li class='footer-nav-item footer-nav-item-4'>
				<a href="/team" title="Team - Batterson Venture Capital">Team</a>
			</li>
			<li class='footer-nav-item footer-nav-item-5'>
				<a href="/current-investments/" title="Portfolio - Batterson Venture Capital">Portfolio</a>
				<ul class="subNavFooter">
					<li class='footer-sub-nav-item-17'>
						<a href="/current-investments/" title="Current Investments">Current Investments</a>
					</li>
					<li class='footer-sub-nav-item-18'>
						<a href="/prior-investments/" title="Prior Investments">Prior Investments</a>
					</li>
				</ul>		
			</li>
			<li class='footer-nav-item footer-nav-item-114'>
				<a href="/news" title="News - Batterson Venture Capital">News</a>	
			</li>
			<li class="otherLinks">
				<a class="investorLogin" href="https://battersonventurepartners.sharefile.com/login.aspx" target="_blank" title="Investor Login">Investor Login</a> 
				<br /> <a href="/site-map" title="Site Map">Site Map</a>
				<br /> <a href="/privacy-policy" title="Privacy Policy">Privacy Policy</a>
				<br /> <a href="/terms-and-conditions/" title="Terms &amp; Conditions">Terms &amp; Conditions</a>
				<br /> <a href="/contact-us" title="Contact Us">Contact Us</a>
			</li>
		</ul>
	</div>
	<div id="siteInfo">
		<p class="tecture">
			<a href="http://tecture.com" title="Chicago Web Design: Tecture">
				<span class="hidden">Chicago Web Design: Tecture</span>
			</a>
		</p>
		<p>Batterson Venture Capital &bull; <?php echo date('Y'); ?> &copy; Copyright &bull; All Rights Reserved</p>
	</div>
</div>
<!-- INSERT GOOGLE ANALYTICS -- >
<!-- Use to input additional scripts to a signal content type -->
	<!-- Anything Slider CSS --> 
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/js/slider/anythingslider.css" rel="stylesheet" type="text/css" />  
	<!-- Anything Slider optional plugins --> 
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/js/slider/jquery.easing.1.2.js" type="text/javascript"></script> 
	<!-- Anything Slider --> 
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/js/slider/jquery.anythingslider.min.js" type="text/javascript"></script> 
	<!-- Anything Slider optional FX extension --> 
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/js/slider/jquery.anythingslider.fx.min.js" type="text/javascript"></script>
  
<script type="text/javascript">



//from int min js
var slideTheArrowNum = 1;
var slideTheArrowHash = window.location.hash;
$(document).ready(function(){
	$("img.modulePointer").css({left:'20px'});
	if(slideTheArrowHash == "#&panel1-2"){ 
             $("img.modulePointer").css({left:'350px'}); 
             slideTheArrowNum = 2;
             $('div.module-20').addClass('active');
             $("div.module-19").css({"background-image":"none"});
	     $("div.module-20").css({"background-image":"url(/assets/root/images/backgrounds/selected_module.jpg)"});
	}
	else if(slideTheArrowHash == "#&panel1-3"){ 
             $("img.modulePointer").css({left:'660px'}); 
             slideTheArrowNum = 3;
             $('div.module-21').addClass('active');
             $("div.module-19").css({"background-image":"none"});
	     $("div.module-21").css({"background-image":"url(/assets/root/images/backgrounds/selected_module.jpg)"});
	}else {
              $('div.module-19').addClass('active');
        }
});




function slideTheArrow(slideDirect){
/*//Note this is an empty stub function based on prev. dev.
	if(slideDirect == "forward"){ slideTheArrowNum++; if(slideTheArrowNum > 3){ slideTheArrowNum = 1; } }
	if(slideDirect == "back"){ slideTheArrowNum--; if(slideTheArrowNum < 1){ slideTheArrowNum = 3; } }
		if(slideTheArrowNum == 1){ $("img.modulePointer").animate({left:'20'}, 500); }
	if(slideTheArrowNum == 2){ $("img.modulePointer").animate({left:'350'}, 500); }
	if(slideTheArrowNum == 3){ $("img.modulePointer").animate({left:'660'}, 500); }

// console.log("slideTheArrow()");
slideTheArrowHash = window.location.hash;
	$("div.module-19, div.module-20, div.module-21").css({"background-image":"none"});
	if(slideTheArrowHash == "#&panel1-1"){
             $("img.modulePointer").animate({left:'350'}, 500);
             $("div.module-20").css({"background-image":"url(/assets/root/images/backgrounds/selected_module.jpg)"}); }
	if(slideTheArrowHash == "#&panel1-2"){
             $("img.modulePointer").animate({left:'660'}, 500);
             $("div.module-21").css({"background-image":"url(/assets/root/images/backgrounds/selected_module.jpg)"}); }
	if(slideTheArrowHash == "#&panel1-3"){
             $("img.modulePointer").animate({left:'20'}, 500);
             $("div.module-19").css({"background-image":"url(/assets/root/images/backgrounds/selected_module.jpg)"}); }
*/
}

//end int min js



$(function(){ 
	currSlide = "";
	slide = "";
	sliderWidth = $('#slider').width();
	sliderHalf = (sliderWidth - 960); 
	sliderHalf = sliderHalf ;

	$('#slider').anythingSlider({ 
	   resizeContents : false,
	   buildNavigation: false,
	   buildArrows: true,
           animationTime: 600,
           onInitialized: function(e,slider){
                var currPage = $('#slider').data('AnythingSlider').currentPage;
                // console.log("start pg " + currPage + " : " + $('#slider').data('AnythingSlider'));
           },
           onSlideBegin:function(e,slider){            
                  var currPage = $('#slider').data('AnythingSlider').targetPage;
                  // console.log("target pg " + currPage);

                  $('.headerSlide').animate({opacity:0},200);
                  $('.bannerTxt').animate({opacity:0},200);
                  $("div.module-19, div.module-20, div.module-21").css({"background-image":"none"});
                  $('div.module').removeClass('active');
                 //Panel Specific code here.
                 if (currPage == 1)
                 {
                      $('.item-19>a>.headerSlide').css({left:0,opacity:0}).delay(300).animate({left:355,opacity:1},700);
                      $('.item-19>a>.bannerTxt').css({left:500,opacity:0}).delay(450).animate({left:260,opacity:1},800);
                      $("img.modulePointer").animate({left:'20'}, 500);
                      $("div.module-19").css({"background-image":"url(assets/root/images/backgrounds/selected_module.jpg)"});
                      $('div.module-19').addClass('active');


                 }else if (currPage ==2)
                 {
                      $('.item-20>a>.headerSlide').css({left:0,opacity:0}).delay(300).animate({left:350,opacity:1},700);
                      $('.item-20>a>.bannerTxt').css({left:500,opacity:0}).delay(450).animate({left:260,opacity:1},800); 
                      $("img.modulePointer").animate({left:'350'}, 500);
                      $("div.module-20").css({"background-image":"url(assets/root/images/backgrounds/selected_module.jpg)"}); 
                       $('div.module-20').addClass('active');
                 }
                 else if (currPage ==3)
                 {
                      $('.item-21>a>.headerSlide').css({left:0,opacity:0}).delay(300).animate({left:350,opacity:1},700);
                      $('.item-21>a>.bannerTxt').css({left:500,opacity:0}).delay(450).animate({left:250,opacity:1},800);
                      $("img.modulePointer").animate({left:'660'}, 500);
                      $("div.module-21").css({"background-image":"url(assets/root/images/backgrounds/selected_module.jpg)"});
                       $('div.module-21').addClass('active');
                 }
           },
           onSlideComplete:function(e,slider){
                  var currPage = $('#slider').data('AnythingSlider').currentPage;
           }
	});
	
	$('.headerSlide').css('left', sliderHalf);
	$('.anythingControls .start-stop').css({'display':'none'});

	
	$(".bannerModules .module").each(function(index){
		index = index+1;
		$(this).attr('rel','#'+index);
	});

	$('.bannerModules .module').click(function(){
		currSlide = $('#slider').data('AnythingSlider').targetPage; // returns page #
		slide = $(this).attr('rel').substring(1);
		// console.log("slide: " + slide + " vs. targetSlide: " + currSlide);
		if(currSlide != slide){
			startSlider(slide, currSlide);		
		}
		return false;
	});

      
});





function startSlider(slide, currSlide){

function slideArrow(page){
// console.log(page);
if (page == 1){
$('.modulePointer').animate({left:'20px'})
$('.module.module-19').css('background-image','url(assets/root/images/backgrounds/selected_module.jpg)');
$('.module.module-20').css('background','transparent');
$('.module.module-21').css('background','transparent');




// console.log("page1");




}else if(page == 2){
$('.modulePointer').animate({left:'350px'})
$('.module.module-19').css('background','transparent');
$('.module.module-20').css('background-image','url(assets/root/images/backgrounds/selected_module.jpg)');
$('.module.module-21').css('background','transparent');




// console.log("page2");
}else{
$('.modulePointer').animate({left:'660px'})
$('.module.module-19').css('background','transparent');
$('.module.module-20').css('background','transparent');
$('.module.module-21').css('background-image','url(assets/root/images/backgrounds/selected_module.jpg)');




// console.log("page3");
}
}
	








 
        // console.log("slide " + slide);
	$('#slider').anythingSlider(slide);
        slideArrow(slide);
	$('.anythingControls .start-stop').css({'display':'none'});
}

</script>
    <?php wp_footer(); ?>
</body>
</html>
