<?php get_header(); ?>
<section id="content" role="main">
	<div class="content-section">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="individual-post" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h1 class="entry-title"><?php the_title(); ?></h1> 
				<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
				<?php the_content(); ?>
				<div class="entry-links"><?php wp_link_pages(); ?></div>
			</div>
			<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
		<?php endwhile; endif; ?>
	</div>
</section>
<?php get_footer(); ?>