<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div id="mainContent">
  <h1 class="int"><?php the_title(); ?></h1>
  <div class="contentBG clearfix">
    <div class="leftColumn full-width">

      <div class="contentContainer">
        <?php the_content(); ?>
      </div>

    </div>
  </div>

</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
