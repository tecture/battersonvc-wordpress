<?php /* Template Name: Business Plan Submit */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div id="mainContent">
  <h1 class="int"><?php the_title(); ?></h1>
  	<div class="contentBG clearfix">
    	<div class="leftColumn">
      		<div class="contentContainer business-plan">
        		<?php the_content(); ?>
        		<div class="contentContainer"><br></div>
      		</div>
    	</div>
    	<div class="rightColumn">
			<div class="module module-113">
				<div>
					<?php the_field('sidebar_content'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	.inputFields .wpcf7-form-control-wrap {
		margin-left: 0;
	}
</style>
<?php endwhile; endif; ?>
<?php get_footer(); ?>