<?php /* Template Name: Sitemap */ ?>
<?php get_header(); ?>
<div id="mainContent">
	<h1 class="int">Site Map</h1>
	<div class="contentBG clearfix">
		<div class="leftColumn">
			<div class="siteMapContainer">

				<div>
					<?php the_field('column_one'); ?>
					<!--<h2>Main</h2>
					<ul>

						<li><a href="/core-elements/" title="Core Elements">Core Elements</a></li>

						<li><a href="/current-investments/" title="Current Investments">Current Investments</a></li>

						<li><a href="/prior-investments/" title="Prior Investments">Prior Investments</a></li>

						<li><a href="/team/" title="Team">Team</a></li>

						<li><a href="/current-investments/" title="Portfolio">Portfolio</a></li>

						<li><a href="/news/" title="News">News</a></li>

						<li><a href="/privacy-policy/" title="Privacy Policy">Privacy Policy</a></li>

						<li><a href="/terms-and-conditions/" title="Terms and Conditions">Terms and Conditions</a></li>
					</ul>-->
				</div>

				<div>
					<?php the_field('column_two'); ?>
					<!--<h2>Team</h2>
					<ul>


					<li>
						<h3>Management</h3>
						<ul>

							<li><a href="/team/len-batterson/" title="Len Batterson">Len Batterson</a></li>

							<li><a href="/team/james-vaughan/" title="James Vaughan">James Vaughan</a></li>

							<li><a href="/team/annie-piotrowski/" title="Annie Piotrowski">Annie Piotrowski</a></li>


						</ul>
					</li>

					<li>
						<h3>Affiliates</h3>
						<ul>



							<li><a href="/team/jonathan-zakin/" title="Jonathan Zakin">Jonathan Zakin</a></li>

							<li><a href="/team/peter-krivkovich/" title="Peter Krivkovich">Peter Krivkovich</a></li>


						</ul>
					</li><li></li><li></li><li></li><li></li><li></li><li></li>
					</ul>-->
				</div>

				<div class="portfolio">
					<h2>Portfolio</h2>
					<ul>

						<li>
							<?php the_field('current_investments'); ?>
							<!--<h3>Current Investments</h3>
							<ul>

								<li><a href="/Pages/Current Investments#Cleversafe Inc" title="Cleversafe Inc">Cleversafe Inc</a></li>

								<li><a href="/Pages/Current Investments#NextGen Solar LLC" title="NextGen Solar LLC">NextGen Solar LLC</a></li>

								<li><a href="/Pages/Current Investments#ThirdStream Bioscience LLC" title="ThirdStream Bioscience LLC">ThirdStream Bioscience LLC</a></li>

							</ul>-->
						</li>

						<li>
							<?php the_field('prior_investments'); ?>
							<!--<h3>Prior Investments</h3>
							<ul>
								<li><a href="/Pages/Prior Investments#US Robotics" title="US Robotics">US Robotics</a></li>

								<li><a href="/Pages/Prior Investments#Nanophase Technologies Corporation" title="Nanophase Technologies Corporation">Nanophase Technologies Corporation</a></li>

								<li><a href="/Pages/Prior Investments#Health Magazine" title="Health Magazine">Health Magazine</a></li>

								<li><a href="/Pages/Prior Investments#America Online Inc" title="America Online Inc">America Online Inc</a></li>

								<li><a href="/Pages/Prior Investments#Proxim Wireless" title="Proxim Wireless">Proxim Wireless</a></li>

								<li><a href="/Pages/Prior Investments#Illinois Superconductor Corporation" title="Illinois Superconductor Corporation">Illinois Superconductor Corporation</a></li>

								<li><a href="/Pages/Prior Investments#Beyond_com Corporation" title="Beyond_com Corporation">Beyond_com Corporation</a></li>

								<li><a href="/Pages/Prior Investments#Larimer Bancorporation Inc" title="Larimer Bancorporation Inc">Larimer Bancorporation Inc</a></li>

								<li><a href="/Pages/Prior Investments#Cybersource Corporation" title="Cybersource Corporation">Cybersource Corporation</a></li>

							</ul>-->
						</li>

					</ul>
				</div>

			</div>
			<div class="contentContainer"><br></div>

		</div>

		<div class="rightColumn">

			<div class="module module-22">

				<div>
					<h3>"<?php the_field("sidebar_qoute"); ?>"</h3>
				</div>

			</div>

		</div>
	</div>
<?php get_footer(); ?>