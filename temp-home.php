<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div id="mainContent" class="home fullPage74">
	<?php 
	$countOne = 19;
	if( have_rows('slider') ): ?>
		<ul id="slider">
    	<?php while ( have_rows('slider') ) : the_row(); 
    		$bannerImage = get_sub_field('banner_image');
    		$backgroundImage = get_sub_field('background_image');
    	?>
    		<li class="sliderItem item-<?php echo $countOne; ?>">
				<img src='<?php echo $backgroundImage['url']; ?>' alt='<?php the_sub_field('lower_title'); ?> image' />
				<a href="<?php the_sub_field('link'); ?>" title="<?php the_sub_field('lower_title'); ?>">
					<div class='headerSlide'>
						<h1>
							<img src='<?php echo $bannerImage['url']; ?>' alt='<?php the_sub_field('lower_title'); ?>' />
						</h1>
					</div>
					<div class='bannerTxt'>
						<div><?php the_sub_field("banner_text"); ?></div>
					</div>
				</a>
			</li>
	    <?php 
	    $countOne++;
	    endwhile; ?>
		</ul>
	<?php endif; ?>
	<?php
	$countTwo = 19;
	if( have_rows('slider') ): ?>
	    <div class="bannerModules clearfix">
		<img class="modulePointer" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/icons/arrow_up.png" alt="module pointer" />
    	<?php while ( have_rows('slider') ) : the_row(); 
    		$bannerImage = get_sub_field('banner_image');
    		$backgroundImage = get_sub_field('background_image');
    	?>
    		<div class="module module-<?php echo $countTwo; ?>" rel="">
				<h2><?php the_sub_field('lower_title'); ?></h2>
				<div class='desc'><?php the_sub_field('lower_description'); ?></div>
			</div>
    	<?php 
	    $countTwo++;
	    endwhile; ?>
		</div>
	<?php endif; ?>
</div>
<?php endwhile; endif; ?>
<div class="mobilePage74 page74">
<h1>Home</h1>
<div class="container">
 <div id="container" class="cf">
 	<?php /*if( have_rows('slider') ): */ ?>
	<div id="main" role="main">
      <section class="slider">
        <div class="flexslider">
        	<?php /* while ( have_rows('slider') ) : the_row(); 
          		$bannerImage = get_sub_field('banner_image');
    			$backgroundImage = get_sub_field('background_image'); */ ?>
	        <div class="flex-viewport" style="overflow: hidden; position: relative;">
	        	<ul class="slides" style="width: 1000%; transition-duration: 0.6s; transform: translate3d(-3690px, 0px, 0px);">
	            	<li data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/mobile/imagine-mobile.jpg" class="">
	            		<p class="content">
	            			<img class="imgTitle" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/banners/imagine-header.png">
	            			It is from those with imagination that our world's growth and possibilities become our truths.
	            		</p>
	  	    			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/banners/imagine-growth-and-possibilities-become-truths.jpg">
					</li>
	            	<li data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/mobile/uncover-mobile.jpg" class="">
	            		<p class="content">
	            			<img class="imgTitle" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/banners/uncover-header.png">
	            			Investment selection is not taken from atop the mountain, but taken from ground level with the soil in our hands.
	            		</p>
	  	    			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/banners/uncover-investment-selection-taken-from-ground-level.jpg">
					</li>
	            	<li data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/mobile/move-mobile.jpg" class="">
	            		<p class="content">
	            			<img class="imgTitle" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/banners/move-header.png">
	           				A driving force will break through any barrier, any tempest, to arrive at its destination.
	           			</p>
	  	    			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/banners/move-break-through-barriers.jpg">
					</li>
				</ul>
			</div>
			<!--<ol class="flex-control-nav flex-control-thumbs">
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/mobile/imagine-mobile.jpg" class=""></li>
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/mobile/uncover-mobile.jpg" class=""></li>
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/root/images/mobile/move-mobile.jpg" class=""></li>
			</ol>
			<ul class="flex-direction-nav">
				<li><a class="flex-prev" href="#">Previous</a></li>
				<li><a class="flex-next" href="#">Next</a></li>
			</ul>-->
		</div>
      </section>
    </div>
	<ul id="mobilehomelist">
		<li>It is from those with imagination that our world's growth and possibilities become our truths. Placing our strength behind breakthrough advancements in biotechnology, communications, healthcare, media, energy, and internet technologies enables innovative technologies to reshape industry, our world, and its people towards a stronger future.</li>
		<li>Our view in investment selection is not taken from atop the mountain, but taken from ground level with the soil in our hands. Early-stage funding commands persistent investigation and creative vision to identify promising investments. Len Batterson's unique insight and perspective of the fluid venture capital landscape have yielded spectacular returns over a 30 year history and our team holds over 100 years of investment and company building experience.</li>
		<li>A driving force will break through any barrier, any tempest, to arrive at its destination. Batterson Venture Capital is the catalyst that propels seed, start-up, and early-stage investment opportunities into the echelon of world-class companies. Our network of industry luminaries provides the entrepreneurs we partner with the tools to achieve levels of success through the utilization of invaluable capabilities and connections.</li>
	</ul>
	<style>
		#mobilehomelist { list-style:none; margin:0; padding:20px; color:#fff; }
		#mobilehomelist li { display:none; }
	</style>
    <script>
    /*
	    $(function(){
	      SyntaxHighlighter.all();
	    });*/
	    /*jQuery(window).load(function() {
	    	if($(window).width < 961) {
			    $('.flexslider').flexslider({
			        animation: "slide",
			        controlNav: "thumbnails",
			        start: function(slider){
			          $('body').removeClass('loading');
			        }
			    });
			 }*/
	    //});
    </script>

</div>
</div>
</div>
<?php get_footer(); ?>